let width = 1420;
let height = 1200;
let boxSize = 20;
let count = 0;
let columns;
let rows;
let current;
let next;

function setup() {
    const cnv = createCanvas(width, height);
    cnv.parent(document.querySelector(".container"));
    img = createImg("images.jpg", 1);
    img.elt.style.display = "none";
    img2 = createImg("images-copy.png", 2);
    img2.elt.style.display = "none";

    columns = Math.floor(width / boxSize);
    rows = Math.floor(height / boxSize);

    current = new Array(columns);
    next = new Array(columns);

    for (let x = 0; x < columns; x++) {
        current[x] = new Array(rows);
        next[x] = new Array(rows);
    }
    init()
    restart()
    question()
}

function init() {
    for (let x = 0; x < columns; x++) {
        for (let y = 0; y < rows; y++) {
            current[x][y] = 0;
            next[x][y] = 0;
        }
    }
}

function draw() {
    board();
    image(img, 1065, 50)
    image(img2, 130, 925)
    //game logic
    for (let x = 0; x < columns; x++) {
        for (let y = 0; y < rows; y++) {
            let neighbors = 0;
            for (let i of [-1, 0, 1]) {
                for (let j of [-1, 0, 1]) {
                    if( i=== j && j ===0 ){
                        continue;
                    }
                    neighbors += current[(x + i + columns) % columns][(y + j + rows) % rows];
                }
            }

            // Rules of Life
            if (x <= columns/2 && x+1 >= columns/2) {
                next[x][y] = 0
            } else if (x >= columns/2 && x+1 <= columns/2) {
                next[x][y] = 0
            } else if (current[x][y] == 1 && neighbors < 2) {
                // Die of Loneliness
                next[x][y] = 0;
            } else if (current[x][y] == 1 && neighbors > 3) {
                // Die of Overpopulation
                next[x][y] = 0;
            } else if (current[x][y] == 0 && neighbors == 3) {
                // New life due to Reproduction
                next[x][y] = 1;
            } else if (x == (columns-1) || y == (rows -1)) {
                //Won't show again on the other side
                next[x][y] = 0;
            } else {
                // Stasis
                next[x][y] = current[x][y];
            }
        }
    }

    [current, next] = [next, current]; 

    //game drawing
    for (let x = 0; x < columns; x++) {
        for (let y = 0; y < rows; y++) {
            if (current[x][y] == 1) {
                if (x >= columns / 2) {
                    fill(255, 255, 255);
                    noStroke();
                } else {
                    fill(0,0,0);
                    noStroke();
                }
                rect(x * boxSize, y * boxSize, boxSize, boxSize);
            }
        }
    }
}


function board() {
    for (let x = 0; x < columns; x++) {
        for (let y = 0; y < rows; y++) {
            if (x >= columns / 2) {
                fill(0, 0, 0);
                noStroke();
                rect(x * boxSize, y * boxSize, boxSize, boxSize)
            } else {
                fill(255, 255, 255)
                noStroke();
                rect(x * boxSize, y * boxSize, boxSize, boxSize)
            }
        }
    }
}

function mouseDragged() {
    noLoop()
    const x = Math.floor(mouseX / boxSize);
    const y = Math.floor(mouseY / boxSize);
    console.log(x,y)

    if (x >= columns / 2) {
        current[x][y] = 1;
        current[columns-x][rows-y] = 1;
        fill(0, 0, 0);
        noStroke();
        rect(x * boxSize, y * boxSize, boxSize, boxSize);
    } else {
        current[x][y] = 1;
        current[columns-x][rows-y] = 1;
        fill(255, 255, 255);
        noStroke();
        rect(x * boxSize, y * boxSize, boxSize, boxSize);
    }
}

function mouseReleased() {
    loop();
}

function mousePressed() {
    init()
    mouseDragged();
}

function question() {
    document.querySelector("canvas").addEventListener("click", () => {
        if (count == 3) {
            count = 0
            init();
            let show = document.querySelector(".text");
            show.style.display = "flex";
            noLoop();
        } else {
            count++;
        }
    })
}

function restart() {
    document.querySelector("button").addEventListener("click", () => {
        let show = document.querySelector(".text");
        show.style.display = "none";
    })
}